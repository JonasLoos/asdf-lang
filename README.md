# ASDF-Lang

Just another programming language in early pre-alpha. Partly functional and inspired by Python, Haskell, JS and others.

As easy as writing `asdf` - as fast as writing `asdf`.

Run `.asdf` files using `./runasdf FILE` (`python3 src/main.py FILE`) or open the REPL with `./repl` (`python3 src/repl.py`).


## Intermediate Interfaces

    file
    ↓ tokenizer
    tokens (token_objects.py)
    ↓ parser
    basic objects (parse_objects.py)
    ↓ objectifier
    typed objects (object.py)
    ↓ optimizer (TODO)
    optimized program (TODO)
    ↓ executer or compiler (TODO)
    print output or compiled program 


## Structure

Code is in `src`, tests in `test` and ideas/thoughts in `doc`.

When calling `runasdf` `main.py` and from there `run.py` is executed. For `repl` it's `repl.py` and then `run.py`.
`run.py` calls `tokenizer.py`, `parser.py`, `objectififer.py`, `optimizer.py` and `executer.py` in this order.


## TODO

* add interface to interal commands (e.g. python-print/input)
* fix Tokenizer allowing `123abc`
* add new executer
	* use `def main` as default entry into program (if it exists)
* add nested objects / data structures (for arrays, strings, ...)
* improve parser
	* parse unary `-`
	* parse array access `arr[i]`
	* fix `integrateBlocks`
	* add `for ... in ... do` loop
	* add `while` loop
	* add function from inline operator
	* add inline functions like `__fun__`
	* add support for other string types (`"` and \`)
* rethink colors in terminal output, maybe add basic code highlightning for error lines
* make `if` work in for loops: such that the resulting list is filtered
* write a compiler, maybe to 'gcc intermediate representation' / LLVM
	* MLIR: hybrid IR, used by TensorFlow, https://mlir.llvm.org/
* write a standard library with commonly used functions
* add an installation script or some packaging stuff
* write everything again in rust (https://github.com/cdisselkoen/llvm-ir) or OCaml
* compile to wasm (using LLVM?)
