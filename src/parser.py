from enum import Enum

from helper import Deque, err, log
from exceptions import ParseError, InternalError
import token_objects as Token
import parse_objects as Parse



def parse(lines, out):
	'''parse token_objects and return a program out of parse_objects'''

	if not lines:
		return None

	# pass to objectifier
	data = basicStuff(lines, out)

	# cluster based on indentation and check for type of subblock
	data = cluster(data)

	# put the blocks into `Do` objects
	data = integrateBlocks(data)

	# line top level parse: `=`, `def`, ...
	# line sub level parse: values (brackets, function application, ...)
	data = topLevelParse(data, last_line_value=False)  # calls all that other stuff

	# create the main program object
	program = Parse.Program(data, out)

	# check for unparsed stuff
	checkForUnparsedObejcts(program)

	# debug
	# log(program.tree())

	# done
	return program


############### basic stuff ###############

class BasicLine:
	'''line object used for parsing, should be replaced by real Objects later'''
	def __init__(self, line_nr, indent, line, out):
		self.line_nr = line_nr
		self.out = out.atLine(line_nr)
		self.indent = indent
		self.line = line
		self.block = Deque()
		self.block_indent = -1
		# if this line extends another expression stuff like `=` and `def` are not allowed
	def __repr__(self):
		return f'{self.line_nr} {list(self.block)}'


def basicStuff(lines, out):
	# make sure there is stuff
	if not lines:
		return []
	basic_lines = [BasicLine(*line, out) for line in lines]
	# make sure the first line has no indentation
	if basic_lines[0].indent != 0:
		basic_lines[0].out.err(f'First code line has indentation, which makes no sense.')
		raise ParseError
	ground_level = BasicLine(-1, -1, [], out)
	ground_level.block_indent = 0
	return [ground_level] + basic_lines



############### cluster ###############

def cluster(lines):
	'''match lines with the same indentation'''

	stack = Deque([lines[0]])
	last_line = lines[0]
	for line in lines[1:]:
		last_indent = stack[-1].block_indent
		if last_indent < line.indent:
			# bigger indentation
			last_line.block_indent = line.indent
			stack += [last_line]
			last_line.block += [line]
		elif last_indent == line.indent:
			# same indentation
			stack[-1].block += [line]
		elif last_indent > line.indent:
			while stack[-1].block_indent > line.indent:
				stack.pop()
			if stack[-1].block_indent == line.indent:
				stack[-1].block += [line]
			else:
				line.out.err(f'Too much or not enough indentation. The line doesn\'t seem to belong any indentation-block.')
		last_line = line

	return stack[0]



############### check for do ###############

class BasicDo:
	'''unparsed do object with indentation-block'''
	def __init__(self, block, out):
		self.block = block
		self.out = out

def integrateBlocks(ground_level):
	"""integrate indentation blocks into the corresponding `do` or add them to the line"""

	# TODO: fix this function, cuz:
	#	x =
	#		somefunction
	#		someValue
	# equals: `x = somefunction somevalue` but should give an error

	lines = Deque()
	for basicline in ground_level.block:
		line = basicline.line
		block = integrateBlocks(basicline)

		# put block into Do object
		if line[-1] == Token.Keyword('do'):
			if not block:
				line[-1].out.err(f'no indentation block after `do` keyword')
				continue
			line[-1] = BasicDo(block, line[-1].out)
		elif line[-1] == Token.Keyword('else'):
			if not block:
				line[-1].out.err(f'no indentation block after single `else` keyword')
				continue
			line += [BasicDo(block, line[-1].out)]
		# add block-lines to line
		else:
			# TODO: this needs a reword, so that it can handle a chain of lines starting with `.`
			line += [x for subline in block for x in subline]
		lines += [line]
	return lines



############### top level parse ###############

def topLevelParse(lines, last_line_value=True):
	'''parse basic line type: Routine-/Function-/Type-Definition and Value'''

	new_lines = Deque()

	# parse all lines except for the last
	for line in (lines[:-1] if last_line_value else lines):
		# Value
		if len(line) < 3:
			new_lines += [newValue(line)]
		# def
		elif Token.Keyword('def') in line:
			new_lines += newRoutineDefinition(line)
		# =
		elif Token.Symbol('=') in line:
			new_lines += newFunctionDefinition(line)
		# : as type definition
		# doesn't make sense at the last line, there it should be handled as value
		elif line[1] == Token.Symbol(':') and isinstance(line[0], Token.Name):
			new_lines += [newTypeDefinition(line)]
		# Value
		else:
			new_lines += [newValue(line)]

	# parse last line (has to be a value if last_line_value is set)
	if last_line_value:
		new_lines += [newValue(lines[-1])]

	return joinIfElseLines(new_lines)


def newRoutineDefinition(line):
	'''create a new RoutineDefinition Object if all requirements are met'''
	def_kw = Token.Keyword('def')
	# fail: def in the middle of a line
	if def_kw in line[1:]:
		token = next(x for x in line[1:] if x == def_kw)
		token.out.err(f'`def` found in the middle of a line.')
		token.out.help(f'`def` has to be at the beginning of a line/statement.')
		return []
	# fail: `=` found
	if Token.Symbol('=') in line:
		token = next(x for x in line[1:] if x == Token.Symbol('='))
		token.out.err(f'`=` and `def` in the same line.')
		token.out.help(f'A `def`-line should end with `do`; `=` is only used for function/constant definition, not for routines.')
		return []
	# fail: `do` missing at the end
	if not isinstance(line[-1], BasicDo):
		line[0].out.err(f'Line starts with `def` but doesn\'t end with `do`.')
		return []
	# fail: no name
	if len(line[1:-1]) == 0:
		line[0].out.err(f'Routine definition without name and arguments.')
		return []
	# fail: non-name-token as name or argument-name
	if not all(isinstance(x, Token.Name) for x in line[1:-1]):
		token = next(x for x in line[1:] if not isinstance(x, Token.Name))
		token.out.err(f'Non name token found in routine definition.')
		return []
	# everything ok
	args = line[2:-1]
	block = topLevelParse(line[-1].block, last_line_value=False)
	return [Parse.RoutineDefinition(line[1], args, block)]

def newFunctionDefinition(line):
	'''create a new FunctionDefinition Object if all requirements are met'''
	equals_pos = line.index(Token.Symbol('='))
	colon_pos = line.index(Token.Symbol(':')) if Token.Symbol(':') in line else equals_pos
	# fail: no function name (and args)
	if equals_pos == 0:
		l[0].out.err(f'`=` at the beginning of a line.')
		l[0].out.help(f'A constant/function name is needed before `=`.')
		return []
	# fail: no function value
	if equals_pos == len(line)-1:
		basicline.out.err(f'No value after `=`.')
		return []
	# fail: function definition inside function definition
	if Token.Symbol('=') in line[equals_pos+1:]:
		token = next(x for x in line[equals_pos:] if x == Token.Symbol('='))
		token.out.err(f'`=` found inside function/constant definition.')
		return []
	# fail: non-name-token as name or argument-name
	if not all(isinstance(x, Token.Name) for x in line[:colon_pos]):
		token = next(x for x in line if not isinstance(x, Token.Name))
		token.out.err(f'Non name token found in function/constant definition.')
		return []
	# everything ok
	name = line[0]
	args = line[1:colon_pos]
	value = newValue(line[equals_pos+1:])
	# parse type
	ftype = [newTypeDefinition(line[:1]+line[colon_pos:equals_pos])] if colon_pos < equals_pos else []
	return ftype + [Parse.FunctionDefinition(name, args, value)]

def newTypeDefinition(line):
	'''create a new TypeDefinition Object if all requirements are met'''
	assert Token.Symbol(':') in line
	colon_pos = line.index(Token.Symbol(':'))
	if colon_pos == 0:
		l[0].out.err(f'`:` at the beginning of a line.')
		l[0].out.help(f'A constant/function name is needed before `:`.')
	elif colon_pos == len(line)-1:
		basicline.out.err(f'No value after `:`.')
	else:
		value = newValue(line[2:])
		return Parse.TypeDefinition(line[0], value)
	return None

def newValue(statement):
	assert statement

	# parse `do`s at the end
	if isinstance(statement[-1], BasicDo):
		statement[-1] = Parse.Do(topLevelParse(statement[-1].block), statement[-1].out)

	# order of execution (each function calls the next):
	# * parseBrackets
	# * parseKeywords
	# * parseInlineOperators
	# * parseHighPrecedenceOperators (`.`,`[]`)
	# TODO: parse unary (`-`) operators
	# * parseFunction
	# * parseValues (ints, strings, ...)

	value = parseBrackets(statement)
	return value



############### bracket parse ###############

def parseBrackets(statement):
	'''parse brackets for a expression'''
	nextParser = parseKeywords

	# dict to get the matching closing bracket
	close = {
		'(': ')',
		'[': ']',
		'{': '}',
	}

	# a stack where all opening brackets are put uppon and removed if the matching closing bracket is encountered
	# the 'ground' level bracket too
	# it's [bracket token, list of tokens inside bracket]
	stack = Deque([[None,Deque()]])

	for elem in statement:

		# non bracket
		if not isinstance(elem, Token.SimpleBracket):
			stack[-1][1].append(elem)
			continue

		# opening bracket
		if elem.raw in close:
			stack.append([elem,Deque()])

		# closing bracket
		elif elem.raw in close.values():
			opening_bracket, values = stack.pop()

			# the closing bracket doesn't match an opening bracket
			if len(stack) == 0 or elem.raw != close[opening_bracket.raw]:
				elem.out.err(f'The closing bracket `{elem.raw}` was found but there wasn\'t an according opening bracket before')
				return None

			# normal bracket ()
			if opening_bracket.raw == '(':
				if not values:
					elem.out.err('No values between the opening and closing bracket: `()`.')
					return None
				stack[-1][1].append(nextParser(values))

			# curly bracket {}
			elif opening_bracket.raw == '{':
				stack[-1][1].append(Parse.BracketCurly(nextParser(values), opening_bracket.out))

			# square brackets []
			else:
				stack[-1][1].append(Parse.BracketSquare(nextParser(values), opening_bracket.out))

		# internal error, unknown bracket
		else:
			raise InternalError(f'`{elem.raw}` (type: {type(elem)}) is declared as bracket, but its not in {[*close, *close.values()]}')

	# not all brackets closed
	if len(stack) > 1:
		stack[-1][0].out.err(f'not all brackets were closed')

	# return the line object in which everything else is (recursivly) contained
	return nextParser(stack[0][1])



############### keyword parse ###############

class BasicIf:
	'''if object used for parsing, should be joined to an Parse.IfElse object later'''
	def __init__(self, condition, value, out):
		self.condition = condition
		self.value = value
		self.out = out

class BasicElif(BasicIf):
	'''elif object used for parsing, should be joined to an Parse.IfElse object later'''
	pass

class BasicElse:
	'''else object used for parsing, should be joined to an Parse.IfElse object later'''
	def __init__(self, value, out):
		self.value = value
		self.out = out

def parseKeywords(statement):
	'''parse keywords like `if`, `else`, ...'''
	nextParser = parseInlineOperators
	Kw = Token.Keyword

	# if
	if Kw('if') in statement:
		if_pos = statement.index(Kw('if'))
		if_out = statement[if_pos].out
		# `do` in the middle of the line
		if Kw('do') in statement[if_pos:]:
			do_pos = if_pos + statement[if_pos:].index(Kw('do'))
			last_do_pos = do_pos
			elifs_found = []
			# parse all `elif`s in the line
			while Kw('elif') in statement[last_do_pos:]:
				elif_pos = last_do_pos + statement[last_do_pos:].index(Kw('elif'))
				if Kw('do') in statement[elif_pos:]:
					last_do_pos = elif_pos + statement[elif_pos:].index(Kw('do'))
					elifs_found += [(elif_pos,last_do_pos)]
				else:
					statement[elif_pos].out.err('Inline `elif` without inline `do`.')
					if isinstance(statement[-1], Parse.Do):
						statement[elif_pos].out.help('Write the corresponding value behind the `do` in the same line or in the next line with indentation.')
					return None
			# combine everything: `if`, `elif`s, `else`
			if Kw('else') in statement[do_pos:]:
				else_pos = do_pos + statement[do_pos:].index(Kw('else'))
				ifelse_obj = nextParser(statement[else_pos+1:])
				if not ifelse_obj:
					statement[else_pos].out.err('Value missing after `else`.')
					return None
				last_else_pos = else_pos
				# handle elifs
				for a, b in ([(if_pos,do_pos)] + elifs_found)[::-1]:
					condition = nextParser(statement[a+1:b])
					if not condition:
						statement[a].out.err(f'Condition missing between `{statement[a]}` and `do`.')
						return None
					value1 = nextParser(statement[b+1:last_else_pos])
					if not value1:
						statement[b].out.err(f'Value missing after `do`.')
						return None
					value2 = ifelse_obj
					ifelse_obj = Parse.IfElse(condition, value1, value2, if_out)
					last_else_pos = a
				return nextParser(statement[:if_pos] + [ifelse_obj])
			# inline `elif` but `else` missing
			elif elifs_found:
				statement[elifs_found[-1][0]].out.err('Inline `elif` found but inline `else` is missing.')
				return None
			# only `if`, `else` should be in the next line
			else:
				condition = nextParser(statement[if_pos+1:do_pos])
				value = nextParser(statement[do_pos+1:])
				new_statement = statement[:if_pos] + [BasicIf(condition, value, if_out)]
				return nextParser(new_statement)
		# `do` from the end of the line (with a following block)
		elif isinstance(statement[-1], Parse.Do):
			condition = nextParser(statement[if_pos+1:-1])
			value = statement[-1]
			new_statement = statement[:if_pos] + [BasicIf(condition, value, if_out)]
			return nextParser(new_statement)
		# `do` missing
		else:
			statement[if_pos].out.err(f'`if` without `do`.')
			statement[if_pos].out.help(f'Every `if` needs a `do` between condition and value.')
			return None

	# elif without if
	elif Kw('elif') in statement:
		elif_pos = statement.index(Kw('elif'))
		# elif at the beginning
		if elif_pos == 0:
			# inline do
			if Kw('do') in statement[1:]:
				do_pos = elif_pos + statement[elif_pos:].index(Kw('do'))
				condition = nextParser(statement[1:do_pos])
				value = nextParser(statement[do_pos+1:])
				return BasicElif(condition, value, statement[0].out)
			# block do
			elif isinstance(statement[-1], Parse.Do):
				condition = nextParser(statement[1:-1])
				value = statement[-1]
				return BasicElif(condition, value, statement[0].out)
			# do missing
			else:
				statement[elif_pos].out.err(f'`elif` without `do`.')
				statement[elif_pos].out.help(f'Every `elif` needs a `do` between condition and value.')
				return None
		# elif in the middle of the line
		else:
			statement[elif_pos].out.err('Lone `elif` in the middle of the line.')
			return None

	# else without if
	elif Kw('else') in statement:
		else_pos = statement.index(Kw('else'))
		if else_pos == 0:
			return BasicElse(nextParser(statement[1:]), statement[0].out)
		else:
			statement[else_pos].out.err(f'Lone `else` in the middle of the line.')
			return None

	# TODO: while, for, ...; maybe in separate function

	# no keywords found
	return nextParser(statement)



############### operator parse ###############

class Order(Enum):
	'''order of evaluation'''
	# order not important
	equal = 0
	# let the Ops function handle it
	combine = 1
	# parse from left to right
	leftright = 2
	# parse from right to left
	rightleft = 3

# operator precedence from low to high
# for binary inline operators
precedence_inline = [
	# token symbol, order of evaluation, fun
	([','], Order.equal),
	(['=>'], Order.leftright),
	([Token.Name('and'),Token.Name('or')], Order.equal),
	(['==','!=','>=','<=','>','<'], Order.combine),
	(['|'], Order.leftright),
	([':'], Order.leftright),
	# TODO: inline functions like `_fun_`
	# TODO: custom inline function symbols
	(['+','-'], Order.equal),
	(['*','/'], Order.equal),
	(['%'], Order.leftright),
	(['^'], Order.rightleft),
]

def parseInlineOperators(statement, curr_precedence=0):
	'''parse the inline operators given in precedence_inline'''

	# empty statement
	if not statement:
		raise InternalError(f'Empty statement: `{statement}` ({curr_precedence})')

	# init
	nextParser = (parseHighPrecedenceOperators
					if curr_precedence >= len(precedence_inline)-1
					else lambda s: parseInlineOperators(s, curr_precedence+1))
	current_symbols, order = precedence_inline[curr_precedence]
	valid_tokens = [Token.Symbol(x) for x in current_symbols]

	# check if there are no operators with the current precedence
	if all(token not in statement for token in valid_tokens):
		return nextParser(statement)

	# init parsing
	positions = [i for i, x in enumerate(statement) if x in valid_tokens]
	operators = [statement[pos] for pos in positions]
	tmp_empty = 'tmp empty value'
	new_statements = [nextParser(statement[i+1:j]) if statement[i+1:j] else tmp_empty for i,j in zip([-1]+positions,positions+[len(statement)])]
	# check if an error occured and return if it did
	if None in new_statements:
		return None
	assert len(positions) == len(new_statements)-1

	# function creation
	if len(positions) == 1 and (new_statements[0] == tmp_empty or new_statements[1] == tmp_empty):
		operators[0].out.err('Function from inline operator not yet implemented.')
		# TODO: add implementation
		return None

	# check for empty statements, i.e. operators following each other
	if new_statements[0] == tmp_empty:
		operators[0].out.err(f'Missing value before `{operators[0]}`')
		return None
	for new_statement, op in zip(new_statements[1:],operators):
		if new_statement == tmp_empty:
			op.out.err(f'Missing value after `{op}`.')
			return None

	# acutally parse the stuff now

	# oder of execution: combine
	if order == Order.combine:
		s = new_statements
		stuff = [Parse.FunctionApplication(Parse.Name(op),[s[i],s[i+1]]) for i, op in enumerate(operators)]
		obj = stuff[0]
		for x in stuff[1:]:
			obj = Parse.FunctionApplication(Parse.Name(Token.Name('and')),[obj,x])
		return obj

	# oder of execution: right to left
	if order == Order.rightleft:
		obj = new_statements[-1]
		for new_statement, op in zip(new_statements[-2::-1], operators[::-1]):
			obj = Parse.FunctionApplication(Parse.Name(op),[new_statement,obj])
		return obj

	# oder of execution: left to right, or equal (i.e. not imporant)
	obj = new_statements[0]
	for new_statement, op in zip(new_statements[1:], operators):
		obj = Parse.FunctionApplication(Parse.Name(op),[obj,new_statement])
	return obj


def parseHighPrecedenceOperators(statement):
	'''e.g. `.`'''
	nextParser = parseFunction

	# dot
	if Token.Symbol('.') in statement:
		# function from inline operator, e.g. `(.value)` used e.g. for `[a,b,c].map (.value)`
		if len(statement) == 2 and isinstance(statement[1], Token.Name):
			statement[0].out.err('Function from inline operator `.` not yet implemented.')
			return None
		else:
			# statement starts with `.`
			if statement[0] == Token.Symbol('.'):
				statement[0].out.err('`.` at a place where is doesn\'t belong.')
				return None
			# main loop searching for `.`s
			if len(statement) >= 3:
				i = 0
				while i + 3 <= len(statement):
					a, b, c = statement[i:i+3]
					if b == Token.Symbol('.'):
						if isinstance(a, Token.Name):
							a = Parse.Name(a)
						if not isinstance(c, Token.Name):
							b.out.err(f'No name after `.` (found `{c}`).')
							statement[-1].out.help('The name of the attribute is missing.')
							return None
						dot_obj = Parse.Dot(a, Parse.Name(c), b.out)
						statement = statement[:i] + [dot_obj] + statement[i+3:]
						i -= 1
					i += 1
			# statement ends with `.`
			if statement[-1] == Token.Symbol('.'):
				statement[-1].out.err('`.` at a place where is doesn\'t belong.')
				if len(statement) > 1 and isinstance(statement[-2], Token.Name):
					statement[-1].out.help('The name of the attribute is missing.')
				return None

	# TODO: `arr[i]`, unary minus `-(a+b)`

	return nextParser(statement)



############### function (/constant) parse ###############

def parseFunction(statement):
	'''parse functions/constants'''
	nextParser = parseValues

	assert statement  # should not be None or empty

	# ->
	if Token.Symbol('->') in statement:
		first_pos = statement.index(Token.Symbol('->'))
		# parse arguments before `->`
		args_a = [Parse.Name(x) if isinstance(x, Token.Name) else nextParser([x]) for x in statement[:first_pos]]
		# check if an error occured
		if None in statement:
			return None
		# check if there is something after the `->`
		if len(statement) - (first_pos+1) <= 0:
			statement[first_pos].out.err('Value missing after `->`.')
			return None
		# parse arguments after `->`
		args_b = parseFunction(statement[first_pos+1:])
		# check if an error occured
		if not args_b:
			return None
		return Parse.FunctionType(args_a, args_b, statement[first_pos])

	# parse all single items
	statement = [Parse.Name(x) if isinstance(x, Token.Name) else nextParser([x]) for x in statement]
	# check if an error occured
	if None in statement:
		return None

	# normal function application
	if len(statement) > 1:
		return Parse.FunctionApplication(statement[0], statement[1:])
	else:
		return statement[0]



############### values parse ###############

def parseValues(statement):
	'''parse values like numbers and strings. This is the last parsing step'''

	# init
	assert len(statement) == 1  # all multitoken stuff should have been parsed before
	elem = statement[0]

	# string
	if isinstance(elem, Token.String):
		if elem.str_type == '\'':
			return Parse.Value(elem.raw, 'Str', elem.out)
		else:
			elem.out.err(f'This string type ({elem.str_type}) is not supported yet.')
			# TODO: add support
			return None

	# number
	if isinstance(elem, Token.Number):
		return Parse.Value(elem.value, 'Num', elem.out)

	# other object
	return elem



############### join if-elif-else ###############

def joinIfElseLines(lines):
	'''join following lines with if (TODO: elif) and else statements.'''
	result = Deque()

	# list of if/Elif objects which will be joined if an else if found
	if_lines = None

	for i, line in enumerate(lines):

		# else
		if isinstance(line, BasicElse):
			if not if_lines:
				line.out.err('`else` without `if` in previous line.')
				continue
			# everything ok -> join if-elif-else lines
			if_else_obj = line.value
			for if_line in reversed(if_lines):
				if_else_obj = Parse.IfElse(if_line.condition, if_line.value, if_else_obj, if_line.out)
			result.append(if_else_obj)
			if_lines = None

		# elif
		elif isinstance(line, BasicElif):
			if not if_lines:
				line.out.err('`elif` without `if` in previous line.')
				continue
			# everything ok -> save line
			if_lines.append(line)

		# if
		elif isinstance(line, BasicIf):
			if if_lines:
				if_lines[-1].out.err('`if` without `else` (onother `if` was found in the following line).')
				if_lines[-1].out.help('Maybe changing the following `if` to an `elif` solves the problem.')
				if_lines = Deque([line])
				continue
			# everything ok -> save line
			if_lines = Deque([line])

		# neither if nor elif nor else
		else:
			if if_lines:
				if_lines[-1].out.err('`if` without (valid) `else`.')
				if_lines = None
			result.append(line)

	# if without else in last line
	if if_lines:
		if_lines[-1].out.err('`if` without (valid) `else`.')

	return result



############### check for unparsed (non Parse) objects ###############

def checkForUnparsedObejcts(program):
	'''check for unparsed objects and print errors if any are found'''

	invalid_objects = program.find(lambda x: not isinstance(x, Parse.Object))
	for invalid_obj in invalid_objects:
		# previous error
		if not invalid_obj:
			continue

		# fail: inline if/elif
		if isinstance(invalid_obj, BasicIf):
			invalid_obj.out.err('Inline `if`/`elif` without inline `else`.')
			invalid_obj.out.help('Make sure the `else` to an inline `if` is in the same line.')
			continue

		# fail: inline else
		if isinstance(invalid_obj, BasicElse):
			invalid_obj.out.errLineOnly('Inline `else` without inline `if`.')
			continue

		invalid_obj.out.err(f'This `{invalid_obj}` is not allowed here.')

	# invalid objects are still inside, but that should be ok
	return program
