class ParseError(Exception):
	'''should be raised if the parsed code contains an error preventing further parsing'''
	pass

class InternalError(Exception):
	'''should be raised if a state, which should never happen, happens'''
	def __init__(self, msg=''):
		self.message = msg
