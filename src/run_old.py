from tokenizer import tokenize
from parser_old import parse
from objectifier_old import objectify
from executer_old import execute
from exceptions import ParseError



def run(lines, out, state = None):
	try:
		# tokenize
		tokens = tokenize(lines, out)
		# parse
		program = parse(tokens, out)
		if out.errors: raise ParseError
		# objectify
		if state == None:
			executable_lines = objectify(program, out)
		else:
			executable_lines, state = objectify(program, out, state)
		if out.errors: raise ParseError
		# execute
		execute(executable_lines, out)
	except ParseError:
		out.print()

	return state
