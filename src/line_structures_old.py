from enum import Enum

from token_objects import *



class LineStructures(Enum):
	name_type_def = [Name, Symbol(':'), Name, '...']
	function_type_def = [Name, Symbol(':'), Name, '...', Symbol('->'), Name, '...']
	name_def = [Name, '...', Symbol('='), [Number, String, Name, Bracket], '...']
	# TODO: constant/function into nothing is useless and should not be executed
	constant_read = [Name]
	# TODO: function into nothing is useless and should not be executed
	function_application = [Name, [Name, Number, String, Bracket], '...']


# TODO: maybe move this file into parser.py
