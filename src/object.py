from enum import Enum
import operator
from types import FunctionType

from exceptions import InternalError
from error_output import InternalErrorOutput
from helper import err, log


class BaseObject:
	'''a base for object-like things: Object, Placeholders, ...'''
	def __init__(self, obj_type, name=None, is_routine=False, out=None, special_object=False, known={}):
		if not special_object:
			if not obj_type and not is_routine:
				raise InternalError('Object without type.')
			if not isinstance(obj_type, Object) and not known:
				raise InternalError('Non-Object type while there are no known-objects.')
		self.type = obj_type if not obj_type or isinstance(obj_type, Object) else Type(obj_type, known)
		self.name = name
		self.is_routine = is_routine
		self.out = out or InternalErrorOutput(self.name)

	def withAttr(self, name=None, is_routine=False, out=None):
		cpy = self.copy()
		if name: cpy.name = name
		if is_routine: cpy.is_routine = is_routine
		if out: cpy.out = out
		return cpy

	def copy(self):
		'''deep copy (except type)'''
		# TODO: check if type has to be deep-copied too: (probably necessary for computed types)
		return BaseObject(self.type, self.name, self.is_routine, self.out, self.special_object)


class CodeBlock(BaseObject):
	def __init__(self, obj, sub_routines):
		super().__init__(obj.type, is_routine=True)
		self.obj = obj
		self.sub_routines = sub_routines

	def __repr__(self):
		return f'<CODEBLOCK [{len(self.sub_routines)}]>'

	def copy(self):
		'''deep copy'''
		return CodeBlock(self.obj.copy(), [x.copy() for x in self.sub_routines])

	def execute(self):
		log(f'execute {type(self).__name__:20}: {self}')
		for sub_routine in self.sub_routines:
			sub_routine.execute()
		return self.obj.execute()


class ArgPlaceholder(BaseObject):
	'''placeholder for an argument; only the type is known and the value gets set during execution'''
	def __init__(self, obj_type, name, out, known={}):
		super().__init__(obj_type=obj_type, name=name, out=out, known=known)
		self.obj = None   # gets set during execution
		self.copied_self = None  # this is used to create copies of self with the possibility of preserving the references

	def __repr__(self):
		return f'<ARG-PLACEHOLDER {self.name}>'

	def copy(self):
		'''deep copy'''
		# this cant be simply copied, as the reference in ObjPlaceholder is then screwed up
		# so use the reference to the already created copy (using copy_ref) instead
		# TODO: This idea is probably broken, as it probably fails for recursive calls,
		# because `copied_self` can be replaced even if not all necessary arg-placeholders are copied/updated yet
		log(f'arg copy: {self.name}')
		if not self.copied_self:
			raise InternalError(f'Trying to copy `{self.name}`, but copied_self is missing.')
		return self.copied_self

	def copy_real(self):
		'''
			deep copy the object and set a reference to the new object in the old one,
			so that other occurences of the old object can use the new object when `copy` is called on them.
		'''
		log(f'arg copy real: {self.name}')
		self.copied_self = ArgPlaceholder(self.type, self.name, self.out)
		self.copied_self.obj = self.obj.copy() if self.obj else None
		return self.copied_self

	def execute(self):
		log(f'execute {type(self).__name__:20}: {self}')
		return self.obj.execute()


class ObjPlaceholder(BaseObject):
	'''
		A placeholder used when not all attributes of the final object are yet known (e.g. only type was defined).
	'''
	def __init__(self, obj_type, name, is_routine=False, out=None, known={}):
		super().__init__(obj_type=obj_type, name=name, is_routine=is_routine, out=out, known=known)
		self.unset = True
		self.obj = None  # gets set later
		self.args = None  # gets set later

	def __repr__(self):
		return f'<OBJ-PLACEHOLDER {self.name}>'

	def set(self, obj, args):
		'''sets the missing attributes of the object from a given object'''
		self.unset = False
		# check type
		obj_type = Type({'inp': [arg.type for arg in args], 'out': obj.type}, known={}) if args else obj.type
		if obj_type != self.type:
			obj.out.err(f'Wrong type when setting `{self.name}`: `{obj.type}` vs. `{self.type}` (actual vs. expected).')
			return None
		# TODO: maybe move argument checking here
		# set the stuff
		assert obj
		self.obj = obj
		assert args != None
		assert all(isinstance(arg, ArgPlaceholder) for arg in args)
		self.args = args  # a list of ArgPlaceholders
		return self

	def copy(self):
		'''deep copy'''
		cpy = ObjPlaceholder(self.type, self.name, self.is_routine, self.out)
		if not self.unset:
			# copy args and object (order might be relevant)
			args = [x.copy_real() for x in self.args]
			obj = self.obj.copy()
			cpy.set(obj, args)
		else:
			raise InternalError('Copying an unset ObjPlaceholder.')
		return cpy

	def execute(self):
		log(f'execute {type(self).__name__:20}: {self}')
		# args were set before in function application
		return self.obj.execute()


class FunctionApplication(BaseObject):
	'''contains an object which is supposed to take in the given args and return a value of the given type'''
	def __init__(self, obj, obj_type, args, is_routine=False):
		super().__init__(obj_type, out=obj.out, is_routine=is_routine)
		# make sure its an ObjPlaceholder (the only object-class that can take arguments)
		if not isinstance(obj, ObjPlaceholder):
			raise InternalError(f'Not a ObjPlaceholder: {obj} ({type(obj)})')
		self.obj = obj
		assert args != None
		assert all(isinstance(arg, BaseObject) for arg in args)
		self.args = args
		self.result = None  # gets set during execution so that this is only executed once

	def __repr__(self):
		return f'<{self.obj}> <{" ".join(x.__repr__() for x in self.args)}>'

	def copy(self):
		'''deep copy'''
		return FunctionApplication(self.obj.copy(), self.type, [x.copy() for x in self.args])

	def execute(self):
		log(f'execute {type(self).__name__:20}: {self}')
		# check if it has to be executed
		if not self.result:
			# make sure the object is set
			if self.obj.unset:
				raise InternalError(f'Executing unset Object: {self.obj}.')
			# copy the object to allow recursion without screwing up the outer calls
			obj = self.obj.copy()
			# set args
			for placeholder_old, value in zip(obj.args, self.args):
				placeholder = placeholder_old.copy()
				if value.is_routine:
					placeholder.obj = value.execute()
				else:
					# lazy evaluation (args not evaluated here but later when they are used)
					placeholder.obj = value
			# execute
			self.result = obj.execute()
		return self.result


class Executable(BaseObject):
	'''an object with an external executable function'''
	def __init__(self, obj_type, fun, **kwargs):
		super().__init__(obj_type, **kwargs)
		self.fun = fun

	def copy(self):
		'''deep copy'''
		return Executable(self.type, self.execute)

	def execute(self):
		log(f'execute {type(self).__name__:20}: {self}')
		return self.fun()


class Object(BaseObject):
	'''an object with a python value'''
	# TODO: move execute into an other object without value
	def __init__(self, obj_type, value, **kwargs):
		super().__init__(obj_type, **kwargs)
		assert not isinstance(value, BaseObject)  # make sure the value is a python value
		self.value = value

	def __repr__(self):
		if self.type == Type.none_type:
			assert self.name == 'Type'
			return '<Type>'
		if not self.type:
			return f'<{self.value}:TYPE IS NONE>'
		pre = ''
		if self.name: pre += f'{self.name}:'
		return f'<{pre}{self.value}:{self.type}>'

	def __eq__(self, other):
		'''check equality'''
		# TODO: maybe add checks for ...
		return (
			isinstance(other,Object) and
			self.type == other.type and
			self.value == other.value)

	def copy(self):
		'''deep copy'''
		return Object(self.type, self.value, name=self.name, is_routine=self.is_routine, out=self.out, special_object=self.special_object)

	def execute(self):
		log(f'execute {type(self).__name__:20}: {self}')
		return self


class Type(Object):
	"""
		a type
		the .type is always 'Type'
		the .value is the name for basic types and a dict of inp&out for function-types
	"""
	none_type = Object(obj_type=None, name='NONE', value=None, special_object=True)
	base_type = Object(obj_type=none_type, name='Type', value={'inp':[],'out': None, 'single-name': 'Type'})

	def __init__(self, value, known, **kwargs):
		'''initialize'''
		name = None
		val = {'inp':[],'out': None, 'single-name': None}
		if not isinstance(value, str) and not isinstance(value, dict):
			raise InternalError('New type from non-string')

		# parse string input
		if isinstance(value, str):
			arr = value.strip().split(' ')
			# non-function type
			if len(arr) == 1:
				if arr[0] == 'Type':
					raise InternalError('Type Type')
				val.update({'single-name': arr[0]})
				name = arr[0]
			# function type
			else:
				if '->' in arr:
					inp_len = arr.index('->')
					fun_inp = [Type(x, known) for x in arr[:inp_len]]
					fun_out = Type(' '.join(arr[inp_len+1:]), known)
					val.update({'inp':fun_inp, 'out': fun_out})
				else:
					raise InternalError(f'Multiple Types without `->`: `{inp}`.')
		# dict input
		else:
			assert value['inp']
			val.update(value)
		super().__init__(name=name, obj_type=self.base_type, value=val, **kwargs)

	def __new__(cls, inp, known, **kwargs):
		'''check if a new object should be created or an existing returned or if something is wrong.'''
		# no input
		if not inp:
			raise InternalError(f'New type from: `{inp}`.')
		# from string
		elif isinstance(inp, str):
			arr = inp.strip().split(' ')
			name = arr[0]
			if len(arr) == 1:
				if name in known:
					return known[name]
			return object.__new__(cls)
		# dict
		elif isinstance(inp, dict):
			# do nothing
			return object.__new__(cls)
		# unrecognized input
		else:
			raise InternalError(f'Unknown object to create a type from: `{inp}`.')

	def __repr__(self):
		# single type
		if self.value['single-name']:
			return f'<{self.value["single-name"]}>'
		# function
		return f'<{" ".join(x.__repr__() for x in self.value["inp"])} -> {self.value["out"]}>'
		return super().__repr__()



def visualize(obj, i=0, printed=[]):
	'''visualize an object and all objects it depends on. Returns a (multi-line) string'''

	# indentation
	ind = '  '*i

	# check for recursive call
	if obj in printed:
		return f'{ind}RECURSIVE CALL: {obj}\n'

	# visualization shortcut
	v = lambda o: visualize(o, i+1, printed+[obj])

	# codeblock
	if isinstance(obj, CodeBlock):
		val = f'{ind}CODEBLOCK:\n'
		for routine in obj.sub_routines:
			val += v(routine)
		return val

	# placeholder
	elif isinstance(obj, ArgPlaceholder):
		return f'{ind}ARG-PLACEHOLDER: {obj.name}\n{ind}ARG-PH-TYPE:\n{v(obj.type)}'
	elif isinstance(obj, ObjPlaceholder):
		val = f'{ind}PLACEHOLDER: {obj.name}\n'
		val += f'{ind}PH-OBJ:\n{v(obj.obj)}'
		if obj.args:
			val += f'{ind}PH-ARGS:\n'
			for arg in obj.args:
				val += v(arg)
		val += f'{ind}PH-TYPE:\n{v(obj.type)}'
		return val

	# function application
	elif isinstance(obj, FunctionApplication):
		val = f'{ind}FUNCTIONAPPLICATION: {"(routine)" if obj.is_routine else ""}\n'
		val += f'{v(obj.obj)}{ind}FA-ARGS:\n'
		for arg in obj.args:
			val += f'{v(arg)}'
		val += f'{ind}FA-TYPE:\n{v(obj.type)}'
		return val

	# executable
	elif isinstance(obj, Executable):
		val = f'{ind}OBJECT: {obj.name or ""} {"(routine)" if obj.is_routine else ""}\n'
		val += f'{ind}OBJ-TYPE:\n{v(obj.type)}'
		return val

	# object
	elif isinstance(obj, Object):
		if obj.type == Type.none_type:
			assert obj.name == 'Type'
			return ind+'OBJECT: Type\n'
		if not obj.type:
			return f'{ind}OBJECT:\n{v(obj.value)}{ind}PY-NONE>\n'
		val = f'{ind}OBJECT: {obj.name or ""} {"(routine)" if obj.is_routine else ""}\n'
		val += f'{ind}OBJ-VALUE:\n{v(obj.value)}'
		val += f'{ind}OBJ-TYPE:\n{v(obj.type)}'
		return val

	# python internal value (e.g. dict, None, ...)
	else:
		return f'{ind}PY: {obj}\n'
