from helper import Deque, err, log
from exceptions import ParseError, InternalError
import parse_objects as Parse
from object import *
from internal_objects import basic, internal, standard



def objectify(program):
	'''convert a program of parse-objects into a program consisting of more general objects with types'''

	# basic checks
	if not program:
		return None
	assert isinstance(program, Parse.Program)

	# multi-level dict to store known objects in
	known_objects = DictStack([basic,standard]).new()

	# objectify and typify the program.
	result = objectifyCodeBlock(program.ground_level_objects, known_objects, should_return_value=False)
	if not result:
		# error occured
		return None
	assert result.type == basic['None']

	# get the main routine
	main = known_objects['main']
	if not main:
		program.out.err('No main routine found.')

	# debug
	print(visualize(main))

	# return main routine
	return main


class DictStack:
	'''stack of dictionaries used to model namespaces'''
	def __init__(self, levels = []):
		self.levels = Deque(levels)
		self.toplevel = self.levels[-1]

	def __getitem__(self, x):
		for level in reversed(self.levels):
			if x in level:
				return level[x]
		return None

	def __setitem__(self, key, value):
		self.toplevel[key] = value

	def __contains__(self, x):
		for level in reversed(self.levels):
			if x in level:
				return True
		return False

	def __add__(self, new_levels):
		return DictStack(self.levels + Deque(new_levels))

	def new(self):
		'''make a copy of self with one extra level on top'''
		return DictStack(self.levels + Deque([{}]))

	def __repr__(self):
		return '\n'.join(x.__repr__() for x in self.levels)


def objectifyStuff(obj, known_objects, inside_routine=False):
	'''
		match names to known_objects,
		check types
		check if object is a routine
		return a single object or None if an error occured
	'''

	###### non-containered objects ######

	# name
	if isinstance(obj, Parse.Name):
		known_obj = known_objects[obj.name]
		# unknown
		if not known_obj:
			obj.out.err(f'Use of `{obj.name}` without previous type definition.')
			return None
		# routine
		if known_obj.is_routine:
			# routine in non-routine context
			if not inside_routine:
				obj.out.err(f'Routine reference `{obj.name}` in non-routine context.')
				obj.out.help(f'Routines such as `{obj.name}` can only be used inside a routine.')
				return None
			# wrap the routine inside a function application object, so that its executed only once
			return FunctionApplication(known_obj, known_obj.type, args=[], is_routine=True)
		# known valid object
		return known_obj

	# value
	if isinstance(obj, Parse.Value):
		return Object(value=obj.value, obj_type=obj.val_type, out=obj.out, known=known_objects)

	###### containered objects ######

	# Do
	elif isinstance(obj, Parse.Do):
		return objectifyCodeBlock(obj.block, known_objects.new(), inside_routine, out=obj.out)

	# BracketCurly
	elif isinstance(obj, Parse.BracketCurly):
		# TODO
		obj.out.err('Not implemented.')
		return None

	# BracketSquare
	elif isinstance(obj, Parse.BracketSquare):
		# TODO
		obj.out.err('Not implemented.')
		return None

	# IfElse
	elif isinstance(obj, Parse.IfElse):
		condition = objectifyStuff(obj.condition, known_objects)
		if not condition:
			# error during previous objectifying
			return None
		if condition.type != basic['Bool']:
			# TODO: convert if possible
			obj.out.err(f'Non-Boolean condition for if statement.')
			return None
		a = objectifyStuff(obj.value1, known_objects)
		b = objectifyStuff(obj.value2, known_objects)
		if not a or not b:
			# error during previous objectifying
			return None
		if a.type != b.type:
			# TODO: convert if possible
			obj.out.err(f'The `if` and `else` blocks have different types.')
			return None
		obj_type = a.type
		# create ifelse object
		is_r = condition.is_routine or a.is_routine or b.is_routine
		return FunctionApplication(internal['ifelse'].withAttr(out=obj.out, is_routine=is_r), obj_type, [condition, a, b])

	# Dot
	elif isinstance(obj, Parse.Dot):
		# TODO
		obj.out.err('Not implemented.')
		return None

	# FunctionType
	elif isinstance(obj, Parse.FunctionType):
		args = [objectifyStuff(arg, known_objects) for arg in obj.pre_args]
		res = objectifyStuff(obj.post_arg, known_objects)
		is_r = any(arg.is_routine for arg in args) or res.is_routine
		return Type({'inp':args,'out':res}, known_objects, out=obj.out, is_routine=is_r)

	# FunctionApplication
	elif isinstance(obj, Parse.FunctionApplication):
		fun = objectifyStuff(obj.function, known_objects, inside_routine)
		args = [objectifyStuff(x, known_objects, inside_routine) for x in obj.args]
		if not fun or not all(args):
			# error during fun/args objectification
			return None
		# if fun is a FunctionApplication without arguments use the function-obj itself
		if isinstance(fun, FunctionApplication):
			if not fun.args:
				fun = fun.obj
		# check arg types
		arg_types_supposed = fun.type.value['inp']
		arg_types_actual = [arg.type for arg in args]
		if arg_types_supposed != arg_types_actual:
			name_tmp = f' of `{fun.name}`' if fun.name else ''
			obj.out.err(f'Argument types{name_tmp} don\'t match: {arg_types_supposed} vs. {arg_types_actual}.')
			return None
		# TODO: add curry
		result_type = fun.type.value['out']
		is_r = fun.is_routine or any(arg.is_routine for arg in args)
		return FunctionApplication(fun, result_type, args, is_routine=is_r)

	# this should never be reached
	raise InternalError(f'Forgot to objectify {obj}.')


def objectifyCodeBlock(lines, known_objects, inside_routine=True, should_return_value=True, **kwargs):
	'''
		objectify the lines of a codeblock of def, do or ground-level
		TODO: parse inner stuff after parsing the top level names, so that later defined functions/... can be referenced before.
	'''

	# init
	routine_calls = Deque()
	objectify_later = Deque()
	def objectifyLater(fun, local_obj):
		'''add something to the list of objects which should be objectified later'''
		objectify_later.append(lambda local_known_objects, local_routine_calls: fun(local_obj, local_known_objects, local_routine_calls, inside_routine))

	# if the block should return a value (i.e. always except for Routines without return type)
	last_line_value = None
	if should_return_value:
		# handle last line separate if it is supposed to hold the return value
		last_line_value = lines[-1]
		lines = lines[:-1]

	# go through lines
	for obj in lines:
		# RoutineDefinition
		if isinstance(obj, Parse.RoutineDefinition):
			# Routine definition e.g. inside function
			if not inside_routine:
				obj.out.err('Routines can\'t be defined when not on ground level or inside a routine.')
				return None
			# (type) not defined before
			if obj.name not in known_objects.toplevel:
				# add routine without args and return value
				known_objects[obj.name] = ObjPlaceholder(obj_type=basic['None'], name=obj.name, is_routine=True, out=obj.out)
			else:
				# make sure that the known object is a ObjPlaceholder
				if not isinstance(known_objects[obj.name], ObjPlaceholder):
					obj.out.err(f'Redefinition of the name `{obj.name}`.')
					return None
			objectifyLater(objectifyRoutineDefinition, obj)

		# FunctionDefinition
		elif isinstance(obj, Parse.FunctionDefinition):
			# (type) not defined before
			if obj.name not in known_objects.toplevel:
				obj.out.err(f'Missing type definition for `{obj.name}`.')
				if obj.name in known_objects:
					obj.out.help('The type definition has to be in the same block.')
				# TODO: infer type if possible
				return None
			else:
				# make sure that the known object is a ObjPlaceholder
				if not isinstance(known_objects[obj.name], ObjPlaceholder):
					obj.out.err(f'Redefinition of the name `{obj.name}`.')
					return None
			objectifyLater(objectifyFunctionDefinition, obj)

		# function (routine) application
		elif isinstance(obj, Parse.FunctionApplication):
			objectifyLater(objectifyFunctionApplication, obj)

		# type def
		elif isinstance(obj, Parse.TypeDefinition):
			# already defined
			if obj.name in known_objects.toplevel:
				obj.out.err(f'The type for `{obj.name}` is already defined.')
				return None
			# objectify the type-value
			obj_type = objectifyStuff(obj.value, known_objects, inside_routine)
			# check for error
			if not obj_type:
				return True
			# set placeholder
			known_objects[obj.name] = ObjPlaceholder(obj_type=obj_type, name=obj.name, known=known_objects, out=obj.out)

		# value/name only line
		else:
			single_obj = objectifyStuff(obj, known_objects, inside_routine)
			# error occured
			if not single_obj:
				return None
			# routine call
			if single_obj.is_routine:
				routine_calls.append(FunctionApplication(single_obj, single_obj.type, []))
			# something stupid
			else:
				single_obj.out.err(f'Non-routine ({type(obj)}) as single statement. This is useless as the return value is thrown away.')
				return None

	# objectify the stuff that was saved for later
	for fun in objectify_later:
		if fun(known_objects, routine_calls):
			# error occured
			return None

	# make sure no ObjPlaceholders exist anymore (objects with type- but without value definition)
	for x in known_objects.toplevel.values():
		if isinstance(x, ObjPlaceholder) and x.unset:
			x.out.err('Unused type-definition.')
			x.out.help(f'If `{x.name}` is needed, make sure it is assigned a value.')

	# handle last line containing the return value
	if should_return_value:
		result = objectifyStuff(last_line_value, known_objects, inside_routine)
		if not result:
			# error during objectification
			return None
	# no return value
	else:
		result = Object(obj_type=basic['None'], value=None, **kwargs)

	# return
	if routine_calls:
		# return a codeblock (containing the routines to call)
		return CodeBlock(result, routine_calls)
	else:
		# return the result value (normal object)
		return result


def objectifyRoutineDefinition(obj, known_objects, routine_calls, inside_routine):
	'''objectify the inner stuff of a routine definition; used for delayed execution; returns True if an error occured'''
	# handle arguments
	known_obj = known_objects[obj.name]
	new_known_objects = known_objects.new()
	args = Deque()
	if not obj.args:
		if known_obj.type.value['inp']:
			obj.out.err('Missing arguments during routine definition.')
			return True
	else:
		if len(obj.args) != len(known_obj.type.value['inp']):
			obj.out.err(f'Number of arguments during routine definition not matching type definition ({len(obj.args)} vs. {len(known_obj.type.value["inp"])}).')
			return True
		for arg, obj_type in zip(obj.args, known_obj.type.value['inp']):
			arg_obj = ArgPlaceholder(obj_type, known_objects, arg.name, arg.out)
			args.append(arg_obj)
			new_known_objects[arg.name] = arg_obj
	# handle code block
	should_return_value = known_obj.type != basic['None']
	new_obj = objectifyCodeBlock(obj.block, new_known_objects, inside_routine=True, should_return_value=should_return_value, name=obj.name, out=obj.out)
	# check if an error occured
	if not new_obj:
		return True
	# set missing values
	if not known_obj.set(new_obj, args):
		# fail (different types)
		return True


def objectifyFunctionDefinition(obj, known_objects, routine_calls, inside_routine):
	'''objectify the inner stuff of a function definition; used for delayed execution; returns True if an error occured'''
	# handle arguments
	known_obj = known_objects[obj.name]
	new_known_objects = known_objects.new()
	args = Deque()
	if not obj.args:
		if known_obj.type.value['inp']:
			obj.out.err('Missing arguments during function definition.')
			return True
	else:
		if len(obj.args) != len(known_obj.type.value['inp']):
			obj.out.err(f'Number of arguments during function definition not matching type definition ({len(obj.args)} vs. {len(known_obj.type.value["inp"])}).')
			return True
		for arg, obj_type in zip(obj.args, known_obj.type.value['inp']):
			arg_obj = ArgPlaceholder(obj_type, arg.name, arg.out, known_objects)
			args.append(arg_obj)
			new_known_objects[arg.name] = arg_obj

	# handle value (stuff after =)
	value = objectifyStuff(obj.value, new_known_objects, inside_routine)
	# check for error during value objectification
	if not value:
		return True
	# check for routine_calls
	if value.is_routine:
		assert inside_routine
		routine_calls.append(value)
	# set missing values
	if not known_obj.set(value, args):
		# fail (different types)
		return True


def objectifyFunctionApplication(obj, known_objects, routine_calls, inside_routine):
	'''objectify the inner stuff of a function application; used for delayed execution; returns True if an error occured'''
	# objectify the object
	fun_obj = objectifyStuff(obj, known_objects, inside_routine)
	# check for error
	if not fun_obj:
		# error during objectifying
		return True
	# not a routine, i.e. useless
	assert isinstance(fun_obj, Object) or isinstance(fun_obj, FunctionApplication)
	if not (fun_obj if isinstance(fun_obj, Object) else fun_obj.obj).is_routine:
		obj.out.err(f'Non-routine ({type(fun_obj)}) as single statement. This is useless as the return value is thrown away.')
		return True
	# everything ok - save routine call
	routine_calls.append(fun_obj)
