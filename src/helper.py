import logging
from collections import deque
from itertools import islice
import sys


logging.basicConfig(level=logging.DEBUG, format='%(filename)s:%(lineno)d \t%(message)s')
logger = logging.getLogger(__name__)

# for usual debugging
log = logger.debug

# for error messages
err = logger.error


class Deque(deque):
	"""deque but with slice functionality"""
	def __getitem__(self, i):
		if type(i) is int:
			return super().__getitem__(i)
		elif type(i) is slice:
			return list(self)[i]
		else:
			raise TypeError(f'sequence index must be integer or slice, not \'{type(i)}\'')
