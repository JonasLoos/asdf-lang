from helper import Deque, err, log
from exceptions import ParseError, InternalError
import parse_objects as Parse
from object import Object, CodeBlock, ObjPlaceholder, ArgPlaceholder, FunctionApplication, Type


def execute(program):
	# TODO: execute
	if program:
		program.execute()
