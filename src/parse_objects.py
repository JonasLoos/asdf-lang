# this file should be imported as `Parse`

from helper import Deque, err, log



class Object:
	def __init__(self, out):
		assert out
		self.out = out


class ContainerObject(Object):
	'''an object containing other Parse objects'''
	def __init__(self, subobjects, out):
		super().__init__(out)
		self.subobjects = subobjects

	# def __contains__(self, elem):
	# 	'''return if an object is inside the current object or inside one of the subobjects'''

	# 	return (
	# 		(any(isinstance(obj, elem) for obj in self.subobjects) if type(elem) == type else elem in self.subobjects)
	# 		or
	# 		any(elem in obj for obj in self.subobjects if isinstance(obj, ContainerObject))
	# 	)

	def __getitem__(self, x):
		return self.subobjects[x]

	def find(self, testFunction):
		'''search recursivly for and return all elements satisfying the given testFunction'''
		result = Deque()
		for elem in self.subobjects:
			if testFunction(elem):
				result.append(elem)
			if isinstance(elem, ContainerObject):
				result += elem.find(testFunction)
		return result

	def tree(self, indentation = 0):
		'''return a tree-string, including all objects inside self'''
		return '  '*indentation + f'{self}\n' + ''.join(obj.tree(indentation+1) if isinstance(obj, ContainerObject) else '  '*(indentation+1)+f'{obj}\n' for obj in self.subobjects)



class Program(ContainerObject):
	'''the whole program (or just a file?)'''
	def __init__(self, ground_level_objects, out):
		super().__init__(ground_level_objects, out)
		self.ground_level_objects = ground_level_objects

	def __repr__(self):
		return f'<PROGRAM {self.out.file}>'



class Do(ContainerObject):
	'''do object used for parsing, should be replaced by a real Object later'''
	def __init__(self, block, out):
		super().__init__([*block], out)
		self.block = block

	def __repr__(self):
		return f'<DO[{len(self.block)}]>'


class RoutineDefinition(ContainerObject):
	'''define a routine which can have side effects and is guaratied to get executed when called'''
	def __init__(self, token, args, block):
		self.name = token.raw
		self.args = [Name(arg) for arg in args]
		self.block = block
		super().__init__([*self.args,*self.block], token.out)

	def __repr__(self):
		return f'<DEF {self.name} DO[{len(self.block)}]>'


class FunctionDefinition(ContainerObject):
	'''define a function (or constant = function without args), which can't have side effects and may be executed lazily or optimized away'''
	def __init__(self, token, args, value):
		self.name = token.raw
		self.args = [Name(arg) for arg in args]
		self.value = value
		super().__init__([*self.args, self.value], token.out)

	def __repr__(self):
		return f'<{self.name}{self.args}={self.value}>'


class TypeDefinition(ContainerObject):
	'''define the type of a function/routine name'''
	def __init__(self, token, value):
		self.name = token.raw
		self.value = value
		super().__init__([self.value], token.out)

	def __repr__(self):
		return f'<{self.name}:{self.value}>'


class BracketCurly(ContainerObject):
	'''{}'''
	def __init__(self, value, out):
		super().__init__([value], out)
		self.value = value

	def __repr__(self):
		return '<{'+f'{self.value}'+'}>'


class BracketSquare(ContainerObject):
	'''{}'''
	def __init__(self, value, out):
		super().__init__([value], out)
		self.value = value

	def __repr__(self):
		return f'<[{self.value}]>'


class IfElse(ContainerObject):
	'''if-else object used for parsing, should be replaced by a real Object later'''
	def __init__(self, condition, value1, value2, out):
		super().__init__([condition, value1, value2], out)
		self.condition = condition
		self.value1 = value1
		self.value2 = value2

	def __repr__(self):
		return f'<IF {self.condition} DO {self.value1} ELSE {self.value2}>'


class Dot(ContainerObject):
	'''`a.b` can be converted to a real Object after/during Typecheck'''
	def __init__(self, obj, attr, out):
		super().__init__([obj, attr], out)
		self.a = obj
		self.b = attr

	def __repr__(self):
		return f'<{self.a}.{self.b}>'


class FunctionType(ContainerObject):
	'''
	class for `->`, which is an inline operator but gets parsed a bit like a function,
	because it takes multiple arguments in front of it
	'''
	def __init__(self, pre_args, post_arg, out):
		super().__init__([*pre_args, post_arg], out)
		self.pre_args = pre_args
		self.post_arg = post_arg

	def __repr__(self):
		return f'<{self.pre_args}->{self.post_arg}>'


class Name(Object):
	'''function/constant name'''
	def __init__(self, token):
		super().__init__(token.out)
		self.name = token.raw

	def __repr__(self):
		return f'<{self.name}>'


class FunctionApplication(ContainerObject):
	'''
	a basic function/constant object.
	Depending on the type this should be reorganized from e.g. `(a b c)` to `(a b) c`
	'''
	def __init__(self, function, args):
		self.function = function
		self.args = args
		super().__init__([self.function, *self.args], function.out)

	def __repr__(self):
		return f'<{self.function}{self.args}>'


class Value(Object):
	'''a basic value like a string or a number'''
	def __init__(self, value, val_type, out):
		super().__init__(out)
		self.value = value
		self.val_type = val_type

	def __repr__(self):
		return f'<{self.value}>'
