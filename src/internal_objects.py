from object import *
from exceptions import InternalError


def create_internal_obj(name, obj_type, execute, is_routine=False):
	if ' -> ' in obj_type:
		arg_types = obj_type.split(' -> ')[0].split(' ')
		res_type = obj_type.split(' -> ')[1]  # works only if there is ONE `->`
	else:
		arg_types = []
		res_type = obj_type
	obj = ObjPlaceholder(name=name, obj_type=obj_type, known=basic, is_routine=is_routine)
	args = [ArgPlaceholder(obj_type=arg_type, name=f'{name}_arg_{i}', out=None, known=basic) for i,arg_type in enumerate(arg_types)]
	inner_obj = Executable(obj_type=res_type, fun=lambda: execute(*args), name=name, is_routine=is_routine, known=basic)
	obj.set(inner_obj, args)
	return obj

def create_internal_binary_fun(name, obj_type, fun):
	execute = lambda a,b: Object(obj_type=obj_type.split(' -> ')[1], value=fun(a.execute().value, b.execute().value), known=basic)
	return create_internal_obj(name, obj_type, execute)

def ifelse_exec(condition, a, b):
	cond = condition.execute()
	if cond == standard['true']:
		return a.execute()
	elif cond == standard['false']:
		return b.execute()
	else:
		raise InternalError(f'`if` argument is neither true nor false: {cond}')

def print_exec(arg):
	print(arg.execute())
	return Object(obj_type=basic['None'], value=None)

def input_exec():
	# TODO: return string option
	try:
		return Object(obj_type='Num', value=float(input()), known=basic)
	except ValueError:
		err('Input was not a Number')
		return Object(obj_type='Num', value=0.0, known=basic)

# some basic types needed by internal/standard objects
basic = {
	'Type': Type.base_type,
	'None': Type('None',{}),
	'Any': Type('Any',{}),
	'Bool': Type('Bool',{}),
	'Num': Type('Num',{}),  # TODO: put this into standard when Any actually takes anything
	'Str': Type('Str',{}),  # TODO: put this into standard when Any actually takes anything
}

# basic functions for internal use only
internal = {
	'ifelse': create_internal_obj('ifelse', 'Bool Num Num -> Num', ifelse_exec)  # TODO: fix type to 'Bool a a -> a'
}

# standard functions regularly used
standard = {
	'print': create_internal_obj('print', 'Num -> None', print_exec, is_routine=True),
	'input': create_internal_obj('input', 'Num', input_exec, is_routine=True),
	'+': create_internal_binary_fun('+', 'Num Num -> Num', lambda a,b: a + b),
	'-': create_internal_binary_fun('-', 'Num Num -> Num', lambda a,b: a - b),
	'*': create_internal_binary_fun('*', 'Num Num -> Num', lambda a,b: a * b),
	'/': create_internal_binary_fun('/', 'Num Num -> Num', lambda a,b: a / b),
	'^': create_internal_binary_fun('^', 'Num Num -> Num', lambda a,b: a ** b),
	'%': create_internal_binary_fun('%', 'Num Num -> Num', lambda a,b: a % b),
	'>': create_internal_binary_fun('>', 'Num Num -> Bool', lambda a,b: a > b),
	'<': create_internal_binary_fun('<', 'Num Num -> Bool', lambda a,b: a < b),
	'<=': create_internal_binary_fun('<=', 'Num Num -> Bool', lambda a,b: a <= b),
	'>=': create_internal_binary_fun('>=', 'Num Num -> Bool', lambda a,b: a >= b),
	'==': create_internal_binary_fun('==', 'Num Num -> Bool', lambda a,b: a == b),
	'!=': create_internal_binary_fun('!=', 'Num Num -> Bool', lambda a,b: a != b),
	# `,` `=>` `and` `or`
	'true': Object(name='true', obj_type='Bool', value=True, known=basic),
	'false': Object(name='false', obj_type='Bool', value=False, known=basic),
}