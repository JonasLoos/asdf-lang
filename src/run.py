from tokenizer import tokenize
from parser import parse
from objectifier import objectify
from optimizer import optimize
from executer import execute
from exceptions import ParseError
from helper import err


def run(lines, out, state = None):

	# run old
	if False:
		from run_old import run as run_old
		return run_old(lines, out, state)

	# temp
	if state:
		err('REPL state isn\'t supported yet.')

	# run
	try:
		# tokenize
		tokens = tokenize(lines, out)

		# parse
		program = parse(tokens, out)
		if out.errors: raise ParseError

		# objectify
		program = objectify(program)
		if out.errors: raise ParseError

		# optimize
		program = optimize(program)
		if out.errors: raise ParseError

		# execute
		execute(program)

	except Exception as e:
		out.print()
		if not isinstance(e, ParseError):
			raise e
