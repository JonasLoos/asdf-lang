import sys
import os.path
import argparse

from helper import err, log
from error_output import ErrorOutput
from run import run


FILE_ENDING = '.asdf'



def main():

	# parse the command line args
	argparser = argparse.ArgumentParser(description=f'Run {FILE_ENDING} files')
	argparser.add_argument('file', help='the file to be parsed and executed')
	argparser.add_argument('--nocolor', help='don\'t use color for errors, warnings and tipps', action='store_false')
	args = argparser.parse_args()
	if args.file[-5:] != FILE_ENDING:
		err(f'provided file name `{args.file}` does not end in `{FILE_ENDING}`')
		exit(1)

	# read input file
	if not os.path.isfile(args.file):
		err(f'the file `{args.file}` cannot be found')
		exit(1) 
	with open(args.file) as f:
		lines = [(line[:-1] if line[-1]=='\n' else line) for line in f.readlines()]

	# init
	out = ErrorOutput(args.file, lines, color=args.nocolor)
	# use file-endings in error output for now, so that terminals can quick open the file-links
	# out = ErrorOutput(args.file[:-len(FILE_ENDING)], lines, color=args.nocolor)

	# run
	run(lines, out)


if __name__ == "__main__":
	main()
