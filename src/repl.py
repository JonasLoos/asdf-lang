import argparse
import os
import readline

from error_output import ErrorOutput
from run import run



def main():

	# parse the command line args
	argparser = argparse.ArgumentParser(description=f'asdf-lang REPL')
	argparser.add_argument('--nocolor', help='don\'t use color for errors, warnings and tipps', action='store_false')
	args = argparser.parse_args()

	# start the repl
	readline.parse_and_bind('')  # add history, and line edit capabilities
	state = {}
	while True:
		try:
			# only print the input prompt if the input is a TTY (and not if it's e.g. piped)
			inp = input('> ' if os.isatty(0) else '')
		except EOFError:
			if os.isatty(0):
				# newline if input is a TTY
				print()
			break
		# init
		out = ErrorOutput('stdin', [inp], color=args.nocolor)
		# run
		run([inp], out, state)


if __name__ == "__main__":
	main()
