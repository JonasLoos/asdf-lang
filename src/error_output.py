from helper import err
from exceptions import InternalError


class ErrorOutput:

	class color:
		HEADER = '\033[95m'
		OKBLUE = '\033[94m'
		OKGREEN = '\033[92m'
		WARNING = '\033[93m'
		FAIL = '\033[91m'
		ENDC = '\033[0m'
		BOLD = '\033[1m'
		UNDERLINE = '\033[4m'


	def __init__(self, file, lines, color=True):
		self.file = file
		self.lines = [line.rstrip().replace('\t', ' ') for line in lines]
		self.errors = []

		if not color:
			# clear colors
			self.color.HEADER = ''
			self.color.OKBLUE = ''
			self.color.OKGREEN = ''
			self.color.WARNING = ''
			self.color.FAIL = ''
			self.color.ENDC = ''
			self.color.BOLD = ''
			self.color.UNDERLINE = ''


	def err(self, msg, line_nr=-1, column_start=-1, column_len=1):
		"""save an error message to print later"""
		self.errors += [[(msg, line_nr, column_start, column_len),[]]]

	def print(self):
		"""print all errors sorted by line number"""
		for err in sorted(self.errors, key=lambda x: x[0][1]):  # sort by line number
			self.print_err(*err[0])
			for help_msg in err[1]:
				self.print_help(help_msg)

	def print_err(self, msg, line_nr, column_start, column_len):
		"""print a colorful error message for errors during parsing"""
		print()
		# print error message
		print(f'{self.color.FAIL}Error: {self.color.BOLD}{msg}{self.color.ENDC}')
		# create position string
		if line_nr >= 0:
			position = f'{self.file}:{line_nr+1} '
			print(f'{self.color.FAIL}{position}{self.lines[line_nr]}{self.color.ENDC}')
			if column_start >= 0:
				print(self.color.FAIL + ' '*(len(position)+column_start) + '^'*column_len + self.color.ENDC)

	def help(self, msg):
		assert self.errors
		self.errors[-1][1] += [msg]

	def print_help(self, msg):
		"""print a colorful help message for errors during parsing"""
		print(f'{self.color.OKGREEN}Tip: {msg}{self.color.ENDC}')

	def atLine(self, line_nr):
		return ErrorOutputAtPos(self, (line_nr,-1,1))

	def atPos(self, pos):
		return ErrorOutputAtPos(self, pos)


class ErrorOutputAtPos:
	def __init__(self, out, pos):
		self._out = out
		assert len(pos) == 3
		self.pos = pos

	def err(self, msg):
		self._out.err(msg, *self.pos)

	def errLineOnly(self, msg):
		self._out.err(msg, self.pos[0])

	def help(self, msg):
		self._out.help(msg)

	def pos(self):
		return self._out.pos(self.pos[0])

	def atPos(self, pos):
		assert len(pos) == 2
		return ErrorOutputAtPos(self._out, (self.pos[0],*pos))


class InternalErrorOutput:
	def __init__(self, name):
		self.name = name
	def err(self, *args):
		raise InternalError(f'Error for internal object `{self.name}`:\n{args}.')
	def help(self, *args):
		pass
