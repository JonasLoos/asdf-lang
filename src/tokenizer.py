import string
from collections import deque

from helper import err, log
import token_objects as Token
from exceptions import ParseError, InternalError



def tokenize(lines, out):
	'''convert the lines of code into an intermediate format consisting of a tuple of line_nr, indentation and token-objects'''

	parsed_lines = deque()

	indents = parseIndentation(lines, out)

	# parse all lines separate
	for i, (indent, line) in enumerate(zip(indents, lines)):
		line = line.strip()

		if not line or line[0] == '#':
			# ignore empty lines and comments
			continue

		# parse line to array of rudimentary objects
		parsed_line = tokenizeLine(line, i, indent, out.atLine(i))
		parsed_lines.append((i, indent, parsed_line))


	return parsed_lines


def parseIndentation(lines, out):
	'''check and parse the indentation amount for the given lines'''

	# counts of lines where the corresponding indentation type is used (including comments)
	space_lines = 0
	tab_lines = 0
	indentations = [0]*len(lines)
	# for each line
	for i, line in enumerate(lines):
		n = len(line)
		rest = len(line.lstrip())
		if n == rest or rest == 0:
			# no indentation or empty line
			pass
		elif (x:=len(line.lstrip(' '))) == rest:
			space_lines += 1
			indentations[i] = n-x
		elif (x:=len(line.lstrip('\t'))) == rest:
			tab_lines += 1
			indentations[i] = n-x
		else:
			out.err('Indentation using spaces and tabs in the same line.', i, 0, n-rest)
			out.help('Use either tabs or spaces for indentation for the whole file. Spaces are recommended.')
			raise ParseError

	if space_lines > 0 and tab_lines > 0:
		# indentation error
		if space_lines < tab_lines:
			out.err('Use of spaces for indentation, while most of the file uses tabs.', i, 0, indentations[i])
		else:
			out.err('Use of tabs for indentation, while most of the file uses spaces.', i, 0, indentations[i])
		out.help('Use either tabs or spaces for indentation for the whole file. Spaces are recommended.')
		raise ParseError

	return indentations


def tokenizeLine(line, line_nr, indent, out):
	'''parse a single line of code without indentation'''
	result = []

	# loop to check for strings to save them from beeing parsed
	mode = 'code'
	last_mode_change = 0
	for i, x in enumerate(line):
		if mode == 'code':
			# currently not inside a string or comment
			if x in '\'"`':
				result += tokenizeStuff(line[last_mode_change:i], line_nr, last_mode_change+indent, out)
				mode = x
				last_mode_change = i+1
			elif x == '#':
				result += tokenizeStuff(line[last_mode_change:i], line_nr, last_mode_change+indent, out)
				last_mode_change = i+1
				mode = 'comment'
				break
			else:
				# char not inside a string -> nothing to do
				pass
		elif mode in '\'"`':
			# currently inside a string
			if mode == x:
				token_out = out.atPos((last_mode_change,i-last_mode_change))
				result += [Token.String(line[last_mode_change:i], x, token_out)]
				last_mode_change = i+1
				mode = 'code'
			else:
				# char inside of the string -> waiting for string end
				pass
		else:
			# this point should never be reached
			raise InternalError(f'tokenizeLine error; mode: `{mode}`, char: `{x}`')

	# open string at line-end
	if mode in '\'"`':
		out.atPos((indent+last_mode_change-1, len(line)-(last_mode_change-1))).err(f'String `{mode}` was not closed')

	# tokenize stuff until the very end of the line
	elif mode == 'code':
		result += tokenizeStuff(line[last_mode_change:], line_nr, last_mode_change+indent, out)

	return result


def tokenizeStuff(stuff, line_nr, startColumn, out):
	'''parse a part of some line of code'''
	# do nothing if stuff is empty
	if not stuff.strip():
		return []

	# rules how to parse characters into tokens (order is relevant)
	cases = [
		# 0: the valid chars, 1: the object types which should be expanded, 2: the type if a new object is created
		(string.ascii_letters,  [Token.Name],                 Token.Name),
		('0123456789',          [Token.Number, Token.Name],   Token.Number),
		('_',                   [Token.Number, Token.Name],   Token.Symbol),
		('.',                   [Token.Number, Token.Symbol], Token.Symbol),
		('+*-/^<>=@:,|&?!~;$%', [Token.Symbol],               Token.Symbol),
		('()[]{}',              [],                           Token.SimpleBracket),
		(' ',                   [],                           None),
	]

	# init
	result = []
	curr_type = None
	curr_obj = ''

	# main loop
	for i, char in enumerate(stuff, startColumn):
		used = False
		for valid_chars, expand_types, new_type in cases:
			if char in valid_chars:
				if curr_type in expand_types:
					# expand current object
					curr_obj += char
				else:
					# add current object to result and create a new one
					if curr_type:
						token_out = out.atPos((i-len(curr_obj),len(curr_obj)))
						result += [curr_type(curr_obj,token_out)]
					curr_type = new_type
					if new_type:
						curr_obj = char
				used = True
				break
		if not used:
			out.atPos((i,1)).err('Invalid character.')

	# last obj
	if curr_type:
		token_out = out.atPos((i-len(curr_obj)+1,len(curr_obj)))
		result += [curr_type(curr_obj,token_out)]

	# extract keywords
	keywords = ['if', 'then', 'elif', 'else', 'for', 'do', 'def', 'as', 'return', 'in']
	result = [Token.Keyword(x.raw, x.out) if isinstance(x, Token.Name) and x.raw in keywords else x for x in result]

	return result

