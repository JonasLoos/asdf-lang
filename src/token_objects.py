class Obj:
	def __init__(self, raw, out=None):  # TODO: use internal out error
		self.raw = raw
		self.out = out

	def __str__(self):
		return self.raw

	def __repr__(self):
		return str(self)


class String(Obj):
	def __init__(self, string, str_type, out=None):
		super(String, self).__init__(string, out)
		self.str_type = str_type

	def __str__(self):
		return f'{self.str_type}{self.raw}{self.str_type}'

	def __eq__(self, other):
		return isinstance(other, String) and self.raw == other.raw


class Name(Obj):
	def __eq__(self, other):
		return isinstance(other, Name) and self.raw == other.raw


class Keyword(Obj):
	def __eq__(self, other):
		return isinstance(other, Keyword) and self.raw == other.raw


class Symbol(Obj):
	def __eq__(self, other):
		return isinstance(other, Symbol) and self.raw == other.raw


class SimpleBracket(Symbol):
	pass

class Number(Obj):
	def __init__(self, number, out=None):
		super(Number, self).__init__(number, out)
		self.value = float(number)

	def __str__(self):
		return str(self.value)

	def __eq__(self, other):
		return isinstance(other, Number) and self.value == other.value


class Bracket(Obj):  # depricated
	def __init__(self, btype, out=None):
		super(Bracket, self).__init__(btype, out)
		self.content = []

	def add(self, elem):
		self.content.append(elem)

	def __contains__(self, elem):
		if elem in self.content:
			return True
		for x in self.content:
			if isinstance(x, Bracket) and elem in x:
				return True

	def __str__(self):
		close = {
			'(': ')',
			'[': ']',
			'{': '}',
		}
		cont = ' '.join(map(str, self.content))
		return f'{self.raw} {cont} {close[self.raw]}'

	def __len__(self):
		return len(self.content)

	def __iter__(self):
		return self.content.__iter__()

	def __getitem__(self, i):
		return self.content[i]

	def index(self, x):
		return self.content.index(x)


class Line(Bracket):  # depricated
	def __init__(self, line_nr, indent, out=None):
		super(Bracket, self).__init__('', out)
		self.line_nr = line_nr
		self.indent = indent
		self.content = []
		self.line_type = ''

	def __str__(self):
		content = ' '.join(map(str, self.content))
		tmp = f'line {self.line_nr}: {content}   '
		space = 40-len(tmp) if len(tmp) < 40 else 0
		return f'{tmp}{space*" "}{self.line_type}'
