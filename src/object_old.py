from enum import Enum
import operator
import types

from exceptions import InternalError
from helper import err, log



class Object:
	def __init__(self, obj_type, properties={}, value=None, name=''):
		self.type = ObjType(obj_type)
		self.prop = properties
		self.value = value
		self.name = name

	def execute(self, args=[], out=None):
		if len(args) != len(self.type.inp):
			err(f'wrong number of args supplied ({len(args)}/{len(self.type.inp)}) for {self} -- Typecheck not yet implemented')
			# raise InternalError f'wrong number of args supplied ({len(args)}/{len(self.type.inp)}) for {self} -- typecheck seems to have failed'
			return None
		if self.value == None:
			if out:
				out.err(f'Object `{self}` is not defined (no value set)')
			else:
				err('no `out` object was given to `execute`')
			return None
		return self.value(self, *args) if type(self.value) is types.FunctionType else self.value

	def __repr__(self):
		name = f'{self.name} : ' if self.name else ''
		value = f' = {self.value}' if self.value else ''
		return f'<{name}{self.type}{value}>'

	def copy(self):
		return Object(self.type, self.prop, self.value, self.name)

	def __eq__(self, other):
		return self.type == other.type and self.prop == other.prop and self.value == other.value and self.name == other.name


class ObjType:
	"""this is the internal representation of the type of an object"""
	inp = []
	out = []
	def __init__(self, value):
		if type(value) is str:
			if value in internal:
				self.out = [internal[value]]
		elif isinstance(value, Object):
			self.out = [value]
		elif type(value) is tuple:
			assert len(value) == 2
			assert type(value[0]) is list
			assert type(value[1]) is list
			assert value[1]
			self.inp = [x.out[0] if x.is_single_val() else x for x in map(ObjType, value[0])]
			self.out = [x.out[0] if x.is_single_val() else x for x in map(ObjType, value[1])]
		elif type(value) is ObjType:
			self.inp = value.inp
			self.out = value.out
		else:
			err(f'Wrong type when creating a new ObjType: {value} : {type(value)}')

	def is_single_val(self):
		return not self.inp and len(self.out) == 1

	def __str__(self):
		inp = ' '.join([(x.name if type(x) is Object else f'({x})') for x in self.inp])
		out = ' '.join([(x.name if type(x) is Object else f'({x})') for x in self.out])
		return f'{inp} -> {out}' if inp else out

	def __repr__(self):
		return str(self)

	def __eq__(self, other):
		other = ObjType(other)
		return self.inp == other.inp and self.out == other.out


class NoneObject(Object):
	def __init__(self):
		self.type = None
		self.prop = {}
		self.value = None
		self.name = None

	def __repr__(self):
		return '<None>'


internal = {}
# internal types
internal['Type'] = Object(NoneObject(), name='Type')
TMP_VALUE = 'tmp value'  # temporary value so that the type value is set
_types = {
	'Num': 'Type',
	'Str': 'Type',
}
for a, b in _types.items():
	internal[a] = Object(internal[b], value=TMP_VALUE, name=a)
# internal functions
_functions = {
	'add': ((['Num','Num'],['Num']), lambda obj, a, b: operator.add(a.execute(), b.execute())),
	'sub': ((['Num','Num'],['Num']), lambda obj, a, b: operator.sub(a.execute(), b.execute())),
	'mul': ((['Num','Num'],['Num']), lambda obj, a, b: operator.mul(a.execute(), b.execute())),
	'div': ((['Num','Num'],['Num']), lambda obj, a, b: operator.truediv(a.execute(), b.execute())),
	'pow': ((['Num','Num'],['Num']), lambda obj, a, b: operator.pow(a.execute(), b.execute())),
	'ifelse': ((['Bool','a','a'],['a']), lambda c, a, b: a if c else b),
}
for f_name, (f_type, f_op) in _functions.items():
	internal[f_name] = Object(f_type, value=f_op, name=f_name)
