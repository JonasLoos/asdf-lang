from collections import deque

from helper import err, log
from object_old import Object, internal
import token_objects as Token
from exceptions import InternalError
from line_structures_old import LineStructures



def objectify(program, out, names=None):
	"""
	Convert the parsed program into a call-tree.
	If `names` is given, continue at the given state (for REPL)
	"""

	give_names_back = names != None
	if names == None:
		names = {}

	# make sure there is something to do
	if not program:
		return ([], names) if give_names_back else []

	names.update(internal)
	executable_lines = deque()
	match = {
		LineStructures.name_type_def: objectify_name_type_def,
		LineStructures.function_type_def: objectify_function_type_def,
		LineStructures.name_def: objectify_name_def,
		LineStructures.constant_read: objectify_constant_read,
		LineStructures.function_application: objectify_function_application,
	}
	for line in program:
		try:
			line_out = out.atLine(line.line_nr)
			result = match[line.line_type](line, names, line_out)
			if result:
				new_names, executable_line = result
				if new_names: names.update(new_names)
				if executable_line: executable_lines.append(executable_line)
		except KeyError as err:
			raise InternalError(f'unknown line structure: `{line.line_type}` / {err} at {line_out.pos()}')

	return (executable_lines, names) if give_names_back else executable_lines


def objectify_name_type_def(line, names, out):
	name = line[0].raw
	if name in names:
		out.err(f'Can\'t set the type of an already defined object')
		out.help(f'set the type before defining your objects')
	else:
		types_out = [parseObjToTypeObj(obj, names, out) for obj in line[2:]]
		new_obj = Object(([], types_out), name=name)
		return {name:new_obj}, None


def objectify_function_type_def(line, names, out):
	name = line[0].raw
	if name in names:
		out.err(f'Can\'t set the type of an already defined objects')
		out.help(f'set the type before defining your objects')
	else:
		split_pos = line.index(Token.Symbol('->'))
		types_inp = [parseObjToTypeObj(obj, names, out) for obj in line[2:split_pos]]
		types_out = [parseObjToTypeObj(obj, names, out) for obj in line[split_pos+1:]]
		new_obj = Object((types_inp, types_out), name=name)
		return {name:new_obj}, None


def objectify_name_def(line, names, out):
	name = line[0].raw
	if name in names:
		obj = names[name]
		if not obj.value:
			# set object value, type is already set
			args_count = len(obj.type.inp)
			if line[args_count+1] != Token.Symbol('=') or any([type(arg) is not Token.Name for arg in line[1:args_count+1]]):
				out.err(f'the definition of `{name}` does not seem to match the given type `{obj.type}`')
				out.help(f'make sure there are as many arguments as the type requires')
				return

			# parse args
			args = dict()
			for arg, arg_type in zip(line[1:], obj.type.inp):
				args[arg.raw] = Object(arg_type, name=arg.raw)

			value = line[args_count+2:]
			assert len(value) >= 1

			local_objects = names.copy()
			local_objects.update(args)
			obj.value = function_application(value, local_objects, args.keys(), out)
		else:
			out.err(f'The object `{name}` is already defined. Redefinition is not permitted.')
			out.help(f'Choose a different name or create a list if you have to do this repeatedly')
	else:
		out.err(f'Unknown name `{name}`')
		out.help(f'Did you set the type of `{name}`?')
		out.help(f'This should look like: `{name} : ...`')



def objectify_constant_read(line, names, out):
	objectify_function_application(line, names, out)


def objectify_function_application(line, names, out):
	# function into nothing is useless and should not be executed
	# but for testing just do it
	# TODO: overthink this
	name = line[0].raw
	if name in names:
		result = function_application(line, names, [], out)  # TODO: is this ok?
		return None, result
	else:
		out.err(f'`{name}` is not defined')





def parseObjToTypeObj(parse_object, known_objects, out):
	# `parse_object` is a Token.Name object with the name of the type
	name = parse_object.raw
	if name in internal:
		return internal[name]
	elif name in known_objects:
		if known_objects[name].type == internal['Type']:
			return known_objects[name]
		else:
			out.err(f'`{name}` is not a type and can\'t be used here')
			return None
	else:
		out.err(f'unknown type `{name}`')
		out.help(f'check for typos and if `{name}` is defined/imported')
		return None

def function_application(value, known_objects, arg_names, out):
	"""TODO: rewrite"""

	# parse expression
	expression = []
	for elem in value:
		if type(elem) is Token.Name:
			name = elem.raw
			if name in known_objects:
				expression.append(known_objects[name])
			else:
				raise InternalError(f'unknown name `{name}` in line {out.pos()}')
		elif type(elem) is Token.Number:
			expression.append(Object('Num', value=elem.value))
		elif type(elem) is Token.Bracket:
			expression.append(Object(
				getBracketType(elem, out),
				value=function_application(elem.content, known_objects, arg_names, out)
			))
		# TODO: other Token objects
		else:
			raise InternalError(f'unknown type `{type(elem)}` in function_application at {out.pos()}')

	if len(expression) == 0:
		raise InternalError(f'Empty expression for `{value}` in line {out.pos()}')

	def real_function_application(object, *args):
		if len(args) != len(arg_names):
			# as long as there is no type check this ca be caused by the asdf program code
			# this also happens for brackets -> this whole function is broken -> TODO
			raise InternalError(f'number of args seems not right ({len(args)}/{len(arg_names)}) for obj {object}')
		# copy objects to not mess them up
		local_objects = dict([(k,x.copy()) for k,x in known_objects.items()])
		assert local_objects == known_objects
		local_expression = [local_objects[x.name] if x.name else x for x in expression]

		if args:
			# function wiht arguments
			# optimize: only update objects wich are used in the expression
			for arg_name, arg_value in zip(arg_names, args):
				assert local_objects[arg_name].value == None # assert args are not set
				local_objects[arg_name].value = arg_value.value

		return local_expression[0].execute(local_expression[1:], out)

	return real_function_application


def getBracketType(bracket, out):
	return 'Num'
