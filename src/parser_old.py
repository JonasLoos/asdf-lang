from collections import deque

from helper import err, log
from line_structures_old import LineStructures
from exceptions import ParseError, InternalError
import token_objects as Token



def parse(lines, out):
	'''take tokens and create lines with some meaning'''

	known_objects = dict()
	parsed_lines = deque()
	parse_err = False

	# try to make sense out of lines
	for line_nr, indent, token_line in lines:
		line_out = out.atLine(line_nr)
		
		# match things to known objects or other things with the same name
		new_objects = dict()
		for i, obj in enumerate(token_line):
			if type(obj) is Token.Name:
				if obj.raw in known_objects:
					token_line[i] = known_objects[obj.raw]
				elif obj.raw in new_objects:
					token_line[i] = new_objects[obj.raw]
				else:
					new_objects[obj.raw] = obj

		# parse brackets
		line = parseBrackets(token_line, Token.Line(line_nr, indent), line_out)

		for line_structure in LineStructures:
			if checkLineStructure(line, line_structure.value):
				line.line_type = line_structure
				break

		# unknown line structure
		if not line.line_type:
			line_out.err(f'Can\'t make sense out of this line. It doesn\'t follow any known line structure.')
			line_out.help('Check for typos')
			parse_err = True

		parsed_lines.append(line)

	if parse_err:
		raise ParseError

	return parsed_lines


def parseBrackets(line, line_obj, out):
	'''check if brackets are used correctly and fill the given line object'''

	close = {
		'(': ')',
		'[': ']',
		'{': '}',
	}

	# a stack where all opening brackets are put uppon and removed if the matching closing bracket is encountered
	# the 'ground' level is the actual line object, which works like a special bracket
	stack = deque([line_obj])
	for elem in line:
		if type(elem) is Token.SimpleBracket:
			# bracket
			if elem.raw in close:
				b = Token.Bracket(elem.raw)
				stack[-1].add(b)
				stack.append(b)
			elif elem.raw in close.values():
				if len(stack) > 1 and elem.raw == close[stack.pop().raw]:
					pass
				else:
					elem.out.err(f'The closing bracket `{elem.raw}` was found but there wasn\'t an according opening bracket before')
					return None
			else:
				raise InternalError(f'`{elem.raw}` is declared as bracket, but its not in {[*close, *close.values()]} at {out.pos()}')
		else:
			# non bracket
			stack[-1].add(elem)

	if len(stack) > 1:
		out.err(f'not all brackets were closed')

	# return the line object in which everything else is (recursivly) contained
	return stack[0]


def checkLineStructure(line, structure):
	'''compare a given line to a given line structure'''

	curr_elem = 0
	for i, check in enumerate(structure):
		if type(check) is str and check == '...':
			# repeat last check
			while curr_elem < len(line) and checkObj(line[curr_elem], structure[i-1]):
				curr_elem += 1
		elif curr_elem < len(line) and checkObj(line[curr_elem], check):
			# check successful
			curr_elem += 1
		else:
			# check failed
			return False

	if curr_elem < len(line):
		# not all elements have been checked
		return False

	# all checks successful
	return True


def checkObj(obj, check):
	'''check if the given object satisfies the structure check'''
	if type(check) is type:
		return type(obj) is check
	elif isinstance(check, Token.Obj):
		return obj == check
	elif type(check) is list:
		# multiple options
		return any([checkObj(obj, c) for c in check])
	else:
		err(f'unknwon structure element `{check}`')
		return False
