# ASDF
easy as asdf
fast as asdf
powerful as A Super Duper Freaking language



# Routine

	def print2 arg1 arg2 as
		print arg1
		print arg2




# Function

	f a b : Int Int -> Int = 42 * a + b

	f : Int Int -> Int
	f a b = 42 * a + b




# If

	asdf = if x==0 then 42 elif x==1 then 0 else 1




# For

	res = for x in [1..] do x^2

	# get alphabet without `q`
	abc = for x in ['a'..'z'] do 
		if x != 'q' then
			x




# Array

	# maybe
    [1,2,3] @ 2
    # or like everywhere
    [1,2,3][2]




# Do

	# calc (42+42)^2
	result = do
		tmp = 42 + 42
		tmp ^ 2   # return last line; like rust




# Code Strings

	class Regex
		# defines rules how to parse a code string

	match_regex str regex : Str Regex -> Bool =
		# apply the regex

	# now the nice shit:
	x = match_regex "just a test" `.*test`
	# x == True
	# the code string `.*test` got parsed using the rules in the Regex class during compile time

	# this could also be used to include code snippets from other languages
	runC `calc_smth();`




# array indexing and slices

	# indexing ideas
	[1,2,3][i]    # like most
	[1,2,3] @ i   # like Eiffel
	[1,2,3] at i  # like Smalltalk (nearly)
	# `@` or `at` are better if you want to use `(@i)` as a function
	# this could be done for `[i]` too: if an array is seen as a function which returns some content if it is given an array, it makes sense

	1..3 == 1,2
	1..=3 == 1,2,3  # like Rust




# multi line strings

like python ''', """ but maybe remove indentation from strings
Or simply multiple lines of normal strings

	mystring = ???
		'Hello,'
		'how are you?'
		'Sincerely, I'
