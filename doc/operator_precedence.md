

precedence: first binds first

* object attribute access `.`, array access `[]`
* unary minus `-`
* normal functions
* `^`
* `%`
* `*` `/`
* `+` `-`
* custom inline functions
* option desintegrator `|`
* inline compare `==` `!=` `>=` `<=` `>` `<`
* inline boolean operators `and` `or`
* lambda `=>`
* `,`
* keywords `if` `elif` `else` ...
* function type definition `->`
* brackets `()`, `[]`, `{}`
* special keyword `def`
* `=`
