# Main question

first or last functions first

haskell: last first?
js: last first?
rust: last first?
python: depends
* first first: most stuff
* last first `map(str, map(ord, 'asdf'))`

## first first:

pro: read the order of execution left->right / top->bot
for consistency most/all functions should be class functions (`class.func`)

	[0,1,2].map (+1)
	res = 'abc'
		.toUpper
		.map ord
		.filter (>65)
		.join ' '  # == '66 67', same below
	res2 = 'abc'.toUpper.map ord).filter (>65)).join ' '

## last first:

pro: see last operation (and final type) first
for consistency `.` should be banned, and class attributes should be accessed like in haskell (`attr class`)

	map (+1) [0,1,2]
	res =
		join ' '
		filter (>65)
		map ord
		toUpper
		'abc'  # == '66 67', same below
	res2 = join ' ' (filter (>65) (map ord (toUpper 'abc'




# space precedence change

how should `abs 1-2` be handled: `(abs 1)-2` (normal precedence) or `abs (1-2)` (how it looks)




# bracket autoclose at the end

make brackets automatically close at the end of the line/expression
this would be similar to haskells `$`
maybe autoopen would be great too: `x = 5 + 5.5).type` or `x = str`




# on

	result1 = (.last) on [1,2,3]
	result2 = [-1]
	# same as: [1,2,3].last

	c = (+2) on 40
	# same as: 40 + 2

	# this is good for
	res = (.last) on
		for x in 1..9 do
			if x%2 == 0 then x^2
	# res == [4,16,36,64]

this is probably unnecessary; just leaving it out is still readable and more consistent




# Meaning of `[`...`]`

1. Arrays/Lists: `[1,2,3]`, `[...'hi', 42]`, `[Num, String, Char]`, `[Num,]`,
2. List access: `[1,2,3][0]`, `arr[i]`, `matrix[0;2]`,
3. List type, when only one Type is inside: `[Num]`, `[(Num, String)]`
	* or maybe better like: `Num[]`,  `(Num, String)[]`




# arrays/matrices with only one element are treated as the element itself

this makes multi-index array access easier/more consistent


	a, b, c = [0,1,2,3,4,5,6][4,2,0]
	# a == 4, b == 2, c == 0
	# this example is for 0 based array indexing

because then the return value of `[???][???]` could always be a list, also for one element

but this is probably stupid




# arrays take over the class functions of its contents

	1.add 2 == 1 + 2
	[1,2,3].add 2 == [1,2,3] + 2 == [1+2,2+2,3+2]

but this would make some nice stuff inconsistent, e.g. `[0]*100` with type (`Num[100]` or `[Num]`)
so probably map and for-loops are good enouth
this could still be implemented in some classes where its good (similar to numpy), no language-level support needed




# { ... | ... }

	myset = { x^2 | x : {1..}; x % 2 == 0 }
	# contains the sqares of all even natural numbers




# always `do` instead of `then`, `as`

	if 42 == 0 do
		# ...
	elif 0 == 0 do
		# ...
	else
		# ...

	def bake bread_type do
		# ...

	for x in 1.. do
		# ...

	result = do
		# ...




# slicing syntax

	arr[a to b step c]
	arr[a..b..c]
	arr[a..b:c]  # this is bad cuz it uses `:`
	arr[a..(a+c)..b]  # haskell style
	arr[a..b!c]
	arr[a..b#c]
	arr[a..b~c]




# type checking

	a : Num
	if a : Num do
		# nice stuff




# lambda

	x = map (a => 2*a+1) [1,2,3]
	x = [1,2,3].map (a => 2*a+1)
	# or
	x = [1,2,3].map (2*_+1)

	x = map (+2) [0,1,2]

	x = map (index _ 'hi there') 'it'  # x == [1,3]
	# same as
	x = map (x => index x 'hi there') 'it'

	x = [1,2,3]
		.filter (>=2)
		.map (*2)
	# x == [4,6]

	x = 'abc'
		.toUpper
		.map (.ascii_code)
		.join ' '
	# x == '65 66 67'




# option class

like rusts `std::option::Option` or haskells `Maybe`-`Just`-`Nothing`

	a : Maybe Int
	a = Maybe 42
	b : Maybe Int
	b = None
	result1 = a | 0  # == 42
	result2 = b | 0  # == 0




# custom symbol inline functions

like `~`, `&`, ...

can be implemented for classes




# `?` `!` as synonym for if else

	x = a ? b ! c
	x = if a do b else c
	# saves 7 char

	y = a ? b ! c ? d ! e
	y = if a do b elif c do d else e
	# saves 11 char

the few chars saved might not be enough to justify the increased complexity and decreased readability (for n00bs)




# `;` to have multiple lines in one

	A : {Num,Str}; someFun : Num A -> A
	# same as
	A : {Num,Str}
	someFun : Num A -> A

	if x = someVar.someAttr; x > 2 and x.even do ...
	if someVar.someAttr > 2 and someVar.someAttr.even do ...

or maybe better: `;` as a way to have subdefinitions
this seems more readable, but the way of execution is from right to left, i.e. against reading-direction
	
	someFun : Num A -> A; A : {Num,Str}
	# there is no real equivalent, cuz in the above line A is hidden from current namespace

	if x > 2 and x.even; x = someVar.someAttr do ...
	if someVar.someAttr > 2 and someVar.someAttr.even do ...




# execute stuff after symbol first

like Haskells `$`, would be unnecessary if autoclosing brackets are a thing

	x = Str $ 40 + 2
	x = Str (40 + 2
	x = Str; 40 + 2




# auto imports

Automatically import some/most/all standard libaries. This can be disabled (e.g. with `noauto`).
Names get automatically overridden if a function is redifined.




# `and` and `or` return the last tested element

	x = [] or [1,2,3]  # == [1,2,3]
	x = 2 and 0 and 1  # == 0

