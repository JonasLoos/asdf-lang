# 0 and half-open ranges

used mostly in computer science
partly used in mathematics (derivation)

Slices: [:a], [a:b], [a:] instead of [:a], [a+1:b], [b+1:]

Ranges are as long as the difference: `len(a..b) == b-a` instead of `b-a+1`

array index represents distance/offset from first element

Matrix ?-th element at row i and column j
	`i*n+j` instead of `(i-1)*n+(j-1)` or `(i-1)*n+j`
Modulo length can be used to get valid indices

good for pointer arithmetic


# 1 and closed ranges

used mostly in mathematics (vector/matrix indices)

easier for noobs (you count 'things' instead of 'offsets')

ranges are more intuitive: 1..10 includes 1 and 10
