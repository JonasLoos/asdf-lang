# general

Run `./test.sh` to run all tests in both normal (main.py) and repl (runasdf.py) mode.

This `test` dir is supposed to be inside the same dir as the `src` dir.

# test structure

each test should be composed of a folder containing the files `in.asdf` and `out` which contain the source code to be executed and the exprected output resprectivly.
