#!/bin/bash

testdir=$(dirname "$(readlink -f $0)")
src="$testdir/../src"
failed_tests=''
for f in "$testdir/"*
do
	if [ -d "$f" ]; then
		echo -n "  testing $(basename "$f")"

		# do the test
		outp=$(diff <(python3 "$src/main.py" "$f/in.asdf") "$f/out") &&
			outp2=$(diff <(cat "$f/in.asdf" | python3 "$src/repl.py") "$f/out") &&
			echo -ne "\r\033[92m✓\033[0m\n" &&
			continue

		# test failed
		echo -ne "\r\033[91mx\033[0m\n"
		if [ -z "$outp2" ]
		then
			echo "error during normal test"
			echo -ne "\n$outp$outp\n\n"
		else
			echo "error during REPL test"
			echo -ne "\n$outp$outp2\n\n"
		fi
		failed_tests="$failed_tests $f"
		break
	fi
done

echo
if [ -z "$failed_tests" ]
then
	echo "all tests successful!"
else
	echo "failed tests:$failed_tests"
fi
